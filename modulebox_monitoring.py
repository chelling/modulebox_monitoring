# modulebox_monitoring: Reads and logs temperature from several
# NTC thermistors, attached to the module testing jigs. NTCs
# are connected to a Raspberry Pi on a 1-wire interface.
#
# Cole Helling, January 2022

import pigpio
import helpers
import os
import glob
import time
import argparse
import json

from datetime import datetime
from datetime import date

parser = argparse.ArgumentParser(
    description = "Read temperature from multiple NTCs and temperature and humidity from an SHT21 sensor.")

parser.add_argument('-c',
                    dest    = 'configfile',
                    type    = str,
                    default = '',
                    help    = 'Path to configuration json.'
                    )

parser.add_argument('-o',
                    dest    = 'logoutput',
                    action  = 'store_true',
                    help    = 'Path to output log file.'
                    )

parser.add_argument('-i',
                    dest    = 'meas_interval',
                    type    = int,
                    default = 10,
                    help    = 'Measurement interval in seconds.'
                   )

parser.add_argument('-t',
                    dest    = 'runtime',
                    type    = int,
                    default = 20,
                    help    = 'Total measurement runtime in minutes.'
                   )
                    

args = parser.parse_args()


if __name__ == "__main__":

  # path to configuration file
  configfile = args.configfile
  # log the ouptut?
  logoutput = args.logoutput

  # measurement interval in seconds
  meas_interval = args.meas_interval
  # total measurement time in minutes
  runtime = args.runtime

  # start time of this script
  start_time = int(time.time())
  # current time
  current_time = start_time
  # end time of this script
  end_time = start_time + runtime * 60
 
  # date_string for measurement output
  # today, format dd-Mon-YYY
  today = date.today().strftime('%d-%b-%Y')
  # time, format HH.MM.SS
  time_str = datetime.fromtimestamp(start_time).strftime('%H.%M.%S')
  date_string = today + '_' + time_str

  try:
    conf = json.load(open(configfile))
  except:
    print("Cannot load configfile, using default values.")
    conf = dict()

  # Sensors
  connected_sensors = conf.get('Sensors', dict()).get('connected_sensors', 'autodiscover')
  # Devices
  oneWire_dir    = conf.get('oneWire_dir', '/sys/bus/w1/devices/')
  Device_aliases = conf.get('Device_aliases', 'autodiscover')
  # Grafana
  grafana_active = conf.get('Grafana', dict()).get('active', True)
  grafana_server = conf.get('Grafana', dict()).get('server', '206.12.94.115')
  grafana_port   = conf.get('Grafana', dict()).get('port', '8186')
  database       = conf.get('Grafana', dict()).get('database', 'telegraf')
 

  # Execute script as long as the current time is less than end time
  while (current_time < end_time):
    # -------------------------------------------------------------- #
    #                           Sensors                              #
    # -------------------------------------------------------------- #
    try:
      pi = pigpio.pi()
      with helpers.TemperatureHumidityMonitor(pi, args) as sht21:

        # if sensors not specified, autodiscover them. Can handle any string
        if (connected_sensors == "autodiscover" or type(connected_sensors) == str):
          connected_sensors = sht21.sensor_autodiscover()
          helpers.edit_Sensors(configfile, connected_sensors)
        
        # store values for logging
        SHT_sensors      = []
        SHT_temperatures = []
        SHT_humidities   = []
        # scan over listed sensors
        for sensor in connected_sensors:

          # currently used to stop spurious results coming from unattached
          # sensors.
          sht21.scan_mux()
          # set proper multiplexer and set channel based on sensor     
          sht21.set_mux(sensor)

          # get temperature and humidity readings from sensor 
          sensor_str = 'SHT%s' %sensor
          temperature = sht21.read_temperature()
          humidity    = sht21.read_humidity()

          # -999 for either values means an error occured, do not write values
          if ((temperature != -999) and (humidity != -999)):
            
            # store values for logging
            SHT_sensors.append(sensor_str)
            SHT_temperatures.append(temperature)
            SHT_humidities.append(humidity)

            # send data to grafana server
            if (grafana_active):
              helpers.SHT_send2influxdb(grafana_server, grafana_port, sensor_str,
                                    temperature, humidity, database)

    except Exception as e:
      print(e)
      print('Error creating connection to i2c.')
 
    # -------------------------------------------------------------- #
    #                           Devices                              #
    # -------------------------------------------------------------- #
    
    # discover devices if none given
    if (Device_aliases == 'autodiscover'):
      Device_aliases = helpers.device_autodiscover(oneWire_dir)
      # if configfile given, edit it with devices found
      if (bool(conf)):
        helpers.edit_Devices(configfile, Device_aliases)

    # to store sensor aliases and temperatures for database upload
    NTC_aliases        = []
    NTC_temperatures   = []

    # loop over devices and grab temperature readings
    for name, alias in Device_aliases.items():
      
      # get temperature
      temp  = helpers.read_temp(oneWire_dir, name)
      # store temperature and alias for grafana
      NTC_temperatures.append(temp)
      NTC_aliases.append(alias)

    if (grafana_active and bool(Device_aliases)):
      helpers.NTC_send2influxdb(grafana_server, grafana_port, NTC_aliases, 
                                NTC_temperatures, database)
 
    # -------------------------------------------------------------- #
    #                           Logging                              #
    # -------------------------------------------------------------- #
    if (logoutput):
      # create file and make header for output file only on first pass
      if (current_time == start_time):
        helpers.start_output_file(date_string, NTC_aliases, SHT_sensors, start_time)
      
      # add values to output file
      helpers.add_values_to_output(current_time, date_string, NTC_aliases, SHT_sensors, 
                                   NTC_temperatures, SHT_temperatures, SHT_humidities)

    # update current time
    current_time = int(time.time())

    # sleep for specified interval time
    time.sleep(meas_interval)
